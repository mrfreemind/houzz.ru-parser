const rp = require('request-promise-native');
const fs = require('fs');
const cheerio = require('cheerio');
const chunk = require('lodash/chunk');
const Promise = require('bluebird');
const Excel = require('exceljs');
const ipc = require('electron').ipcRenderer;
const shell = require('electron').shell
const os = require('os')

let folder = '';
let fullPath = '';
let currentDate;

const selectDirBtn = document.getElementById('select-directory');

selectDirBtn.addEventListener('click', function (event) {
  ipc.send('open-file-dialog')
})

ipc.on('selected-directory', function (event, path) {
  document.getElementById('selected-file').innerHTML = `Путь к сохранённым файлам: ${path}`;
  folder = path[0];
  var div = document.getElementById("main-div");
  div.style.display = "block";
})

ipc.on('information-dialog-selection', function (event, index) {
  // shell.openItem(fullPath)
  // shell.showItemInFolder(fullPath + '/');
})

const categories = [
  {
    id: 1,
    name: `Архитекторы и архитектурные бюро`,
    url: `https://www.houzz.ru/professionals/Arkhitektory`,
    active: false
  },
  {
    id: 2,
    name: `Дизайнеров интерьера и декораторов`,
    url: `https://www.houzz.ru/professionals/dizayn-interyera-dekor`,
    active: false
  },
  {
    id: 3,
    name: `Мебель и аксессуары`,
    url: `https://www.houzz.ru/professionals/mebely-i-aksessuary`,
    active: false
  },
  {
    id: 4,
    name: `Кухни на заказ`,
    url: `https://www.houzz.ru/professionals/proizvodstvo-i-ustanovka-kuhony`,
    active: false
  }
];

categories.map(category => {
  const data = `
  <div>
    <input onchange="parser.changeCheckbox(this.value)" type="checkbox" id="checkbox_category_${category.id}" name="category_${category.id}" value="${category.id}">
    <label for="category_${category.id}">${category.name} <span id="span_category_${category.id}"></span></label>
  </div>
  `;
  document.getElementById(`categories`).innerHTML += data;
})

const changeCheckbox = (id) => {
  const category = categories.filter(category => +category.id === +id)[0];
  if (!category) console.log('not found category')
  category.active = !category.active;
}

const startParse = async () => {
  currentDate = new Date();
  const chosenCategories = categories.filter(category => category.active);
  console.log(chosenCategories);
  const fullPath = `${folder}/${currentDate.getFullYear()}-${currentDate.getMonth() + 1}` +
    `-${currentDate.getDate()}-${currentDate.getHours()}-${currentDate.getMinutes()}` +
    `-${currentDate.getSeconds()}`
  fs.mkdir(fullPath, function(e){
    if(!e || (e && e.code === 'EEXIST')){
        console.log('folder created');
    } else {
        //debug
        console.log('folder not created');
        console.log(e);
    }
  });

  // console.log(`Before Promise.map`);
  await Promise.map(chosenCategories, async (category, id) => {
    // console.log(`We are inside promise.map category id = ${category.id}`);
    return new Promise(async (resolve, reject) => {
      const mainPages = [];

      const categoryPath = `${fullPath}/${category.name}`;

      fs.mkdir(categoryPath, function(e){
        if(!e || (e && e.code === 'EEXIST')){
            console.log('folder created');
        } else {
            //debug
            console.log('folder not created');
            console.log(e);
        }
      })

      // console.log(`We are inside promise category id = ${category.id}`);
      const userAmount = await getAmount(category.url);
      document.getElementById(`log`).innerHTML += `<p>Начали парсинг категории ${category.name}</p>`;
      document.getElementById(`log`).innerHTML += `<p>Количество экспертов в категории ${category.name} - ${userAmount}</p>`;
      document.getElementById(`span_category_${category.id}`).innerHTML = userAmount;
      // console.log(`We are finished promise category id = ${category.id}`);
      const pageAmount = Math.ceil(userAmount / 15);
      document.getElementById(`log`).innerHTML += `<p>Количество страниц в категории ${category.name} - ${pageAmount}</p>`;

      let counter = 0;
      while (counter < pageAmount) {
        mainPages.push(`${category.url}/p/${counter * 15}`);
        counter++;
      }

      const users = [];

      let donePageCount = 0;
      let failPageCount = 0;
      await Promise.map(chunk(mainPages, 1000), async (chunk, id) => {
        console.log(`Delaem chunk ${id}`);
        try {
          await Promise.all(chunk.map(link => {
            return new Promise(async (resolve, reject) => {
              console.log('we are inside');
              try {
                await rp(link)
                  .then(body => {
                    const $ = cheerio.load(body);
                    $(`.whiteCard.pro-card`).map((index, element) => {
                      const user = {};
                      try {
                        user.name = $(element).find(`.pro-title`).text();
                      } catch (e) {
                        user.name = 'Нет имени';
                      }
                      console.log(user.name);
                      try {
                        user.phone = $(element).find(`.pro-phone > span`).text();
                      } catch (e) {
                        user.phone = 'Нет телефона';
                      }
                      try {
                        user.address = $(element).find(`.pro-location > span.pro-list-item--text`)[0].children[0].children[0].data;
                      } catch (e) {
                        user.address = 'Нет адреса';
                      }
                      try {
                        user.url = $(element).find(`.pro-title`).attr('href');
                      } catch (e) {
                        user.url = 'Нет ссылки?';
                      }
                      users.push(user);
                    })
                    document.getElementById(`span_category_${category.id}`).innerHTML = `${(donePageCount + 1) * 15} / ${userAmount}`;
                    console.log(`donePageCount = ${donePageCount}`);
                    console.log(`failPageCount = ${failPageCount}`);
                    donePageCount++;
                    resolve(true);
                  })
              } catch (e) {
                console.log(`donePageCount = ${donePageCount}`);
                console.log(`failPageCount = ${failPageCount}`);
                console.log(e);
                failPageCount++;
                reject(`${link} - vse huevo`);
              }
            })
          }));
        } catch (e) {
          console.log(e);
        }
      }, {concurrency: 1});

      console.log(`donePageCount = ${donePageCount}`);
      console.log(`failPageCount = ${failPageCount}`);
  
      console.log('Начинаем делать таблицу');
  
      chunk(users, 10000).map(async (chunk, id) => {
        console.log(`Делаем таблицу id = ${id}`);
        const workbook = new Excel.Workbook();
        const sheet = workbook.addWorksheet('My Sheet');
  
        sheet.columns = [
          { header: 'Категория', key: 'id', width: 20 },
          { header: 'ФИО', width: 40 },
          { header: 'Город', width: 15 },
          { header: 'Телефон', width: 20 },
          { header: 'URL', width: 30 }
        ];
  
        const rowArray = [];
        chunk.map(element => {
          rowArray.push([category.name, element.name, element.address, element.phone, element.url])
        })
  
        rowArray.map(arr => sheet.addRow(arr));
  
        try {
          await new Promise((resolve, reject) => {
            workbook.xlsx.writeFile(`${categoryPath}/${category.name}_${id * 10000}_${(id + 1) * 10000}.xlsx`)
              .then(function () {
                resolve(true);
              });
          });
        } catch (e) {
          console.log(e);
        }
      })

      resolve(true);
    });
  }, {concurrency: 1});
  ipc.send('open-information-dialog');
  shell.openItem(fullPath + '/');
}

const getAmount = async (url) => {
  let categoryMainPageBody;
  try {
    categoryMainPageBody = await rp(url)
      .then(body => {
        return body;
      })
  } catch (e) {
    return console.log(e);
  }
  const $ = cheerio.load(categoryMainPageBody);
  const regex = /[\d|,|.| |e|E|\+]+/g;
  const userAmount = $(`h1.header-2.main-title`)[0].children.map(child => child.data).join('');
  return +userAmount.match(regex).join('');
}

module.exports = {
  startParse,
  changeCheckbox
};